﻿//Server
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;


public class UdpServer
{
    public static char getNextChar(char c)
    {
        int ascii = (int)c;
        int nextAscii = ascii + 4;
        char nextchar = (char)nextAscii;
        return nextchar;
    }
    public static void Main()
    {
        string input;
        byte[] data = new byte[1024];
        byte[] data1 = new byte[1024];
        //membuka IP dan mengatur Port
        IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 8001);
        //mendefinisikan jenis protokol adalah UDP
        UdpClient newsock = new UdpClient(ipep);
        //input username
        Console.Write("Masukkan username anda : ");
        string username = Console.ReadLine();
        Console.WriteLine("Menunggu klien...");
        //menerima 
        IPEndPoint from = new IPEndPoint(IPAddress.Any, 0);
        data = newsock.Receive(ref from);
        string clientUsername = Encoding.ASCII.GetString(data, 0, data.Length);
        //menuliskan IP client
        Console.WriteLine("Menerima dari : {0}", from.ToString());
        //menuliskan username client
        Console.WriteLine("Username Client : " + Encoding.ASCII.GetString(data, 0, data.Length));
        //mengirim data
        data = Encoding.ASCII.GetBytes(username);
        newsock.Send(data, data.Length, from);

        while (true)
        {
            //menerima
            data = newsock.Receive(ref from);
            /*char nextChar;
            char letter = 'a';
            if (letter == 'z')
                nextChar = 'd';
            else if (letter == 'Z')
                nextChar = 'A';
            else nextChar = (char)(((int)letter) + 4);*/
            string datastring = Encoding.ASCII.GetString(data, 0, data.Length);
            string modified = "";
            for (int i = 0; i < datastring.Length; i++)
            {
                int c = getNextChar(datastring[i]);
                char d = (char)(c);
                modified += d;
                
            }
            Console.WriteLine(clientUsername + " : " + Encoding.ASCII.GetString(data, 0, data.Length));
            Console.WriteLine(username + " : " + modified);
            byte[] sent = Encoding.ASCII.GetBytes(modified);
            


            //mengirim
            //input = Console.ReadLine();
            //data1 = Encoding.ASCII.GetBytes(input);
            newsock.Send(sent, sent.Length, from);
        }
    }
}
