# UDP Convert Char
#**Praktikum Jaringan Komputer Semester 3**

4210181013_Faidurrohman
4210181024_Frederiko Adrian

**Deskripsi**
1. masuk dengan username
2. client mengirim string kepada server
3. server menerima dan megubahnya ke int
4. server menggunakan rumus dengan mengubah index ASCII karakter yang diterima dengan +4
5. server mengubah int ke char
6. server mengirim byte karakter dengan index ASCII yang baru
7. client menerima hasil dan dikonversi ke string dan ditampilkan


