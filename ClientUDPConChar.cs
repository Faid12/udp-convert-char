﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class UdpClient
{
    public static void Main()
    {
        byte[] data = new byte[1024];
        string input, stringData;
        IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8001);
        Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        //Input Userename dan kirim
        Console.Write("Masukkan username anda : ");
        string username = Console.ReadLine();
        data = Encoding.ASCII.GetBytes(username);
        server.SendTo(data, data.Length, SocketFlags.None, ipep);
        //menerima
        IPEndPoint from = new IPEndPoint(IPAddress.Any, 0);
        EndPoint control = (EndPoint)from;

        int recv = server.ReceiveFrom(data, ref control); //digunakan untuk getstring
                                                          //menuliskan IP server
        Console.WriteLine("Menerima pesan dari : {0}", control.ToString());
        string serverUsername = Encoding.ASCII.GetString(data, 0, recv);
        //menuliskan username server
        Console.WriteLine("Username Server : " + serverUsername);

        while (true)
        {
            Console.Write(username + " : ");
            input = Console.ReadLine();
            if (input == "exit")
                break;
            server.SendTo(Encoding.ASCII.GetBytes(input), control);
            data = new byte[1024];
            recv = server.ReceiveFrom(data, ref control);
            stringData = Encoding.ASCII.GetString(data, 0, recv);
            Console.WriteLine(serverUsername + " : " + stringData);
        }
        Console.WriteLine("Menghentikan klien");
        Console.ReadKey();
        server.Close();
    }
}
